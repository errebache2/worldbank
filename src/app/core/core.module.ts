import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { ContriesComponent } from './components/contries/contries.component';
import { IndicatorsComponent } from './components/indicators/indicators.component';
import { IndicatorsGraphComponent } from './components/indicators-graph/indicators-graph.component';



@NgModule({
  declarations: [
    DashboardComponent,
    HeaderComponent,
    ContriesComponent,
    IndicatorsComponent,
    IndicatorsGraphComponent,
  ],
  imports: [
    CommonModule
  ],
  exports:[
    DashboardComponent,
    HeaderComponent,
    ContriesComponent,
    IndicatorsComponent,
    IndicatorsGraphComponent,
  ]
})
export class CoreModule { }
